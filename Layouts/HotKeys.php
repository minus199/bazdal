<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/17/14
 * Time: 2:15 PM
 */
?>

        <div id="hotKeys">
            <!--<img id="keyMapTrigger" src="Public/IMG/bubble-question-mark-hi.png" alt="Hotkeys Map" width="20px" height="40px">-->
            <img id="keyMapTrigger" class="hoverButtons" src="Public/IMG/qm2.png" >
            <table id="keyMap" >
                <tr><td class="code">[SHIFT] + h</td><td>Toggle this info</td></tr>
                <tr><td class="code">[CTRL] + [ENTER]</td><td>Submit translation</td></tr>
                <tr><td class="code">[SHIFT] + &#x25B2;</td><td>Open string list</td></tr>
                <tr><td class="code">[SHIFT] + &#x25BC;</td><td>Close string list</td></tr>
                <tr><td class="code">[1-9]</td><td>Open Corresponding Panel (i.e. 1 for settings)</td></tr>
            </table>
        </div>
