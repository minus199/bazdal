<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/17/14
 * Time: 2:17 PM
 */
?>

        <div id="mainPanel">
            <span>Settings</span>
            <div>
                <table id="stringFiles" >
                    <Tr>
                        <td rowspan="100%"><div id="fileuploader">Upload</div></td>
                        <td rowspan="100%"><B>Files</B></td>
                    </Tr>
                    <tr>
                        <td><u>Source:</u></td>
                        <td><select id="srcFile"></select></td>
                        <td rowspan="100%">
                            <a id="downloadLinks">Complete</a>
                        </td>
                    </tr>
                    <tr>
                        <td><u>Target:</u></td>
                        <td><select id="transFile"></select></td>
                        <td rowspan="100%"><label for="stringFiles"></label></td>
                    </tr>
                </table>
            </div>

            <span style="position: relative; display: inline">Translation Panel</span>
            <div>
                <table id="main1Panel">
                    <tr>
                        <td><input disabled id='currentName' type="text" placeholder="Current Name..." /></td>
                        <td rowspan="2"><textarea id="targetString" placeholder="Target String"></textarea></td>
                    </tr>
                    <tr><td><textarea id="sourceString" disabled placeholder="Source String"></textarea></td></tr>
                    <tr><td colspan="100%" style="text-align: center;"><?php include_once 'Layouts/StatsAndToggles.php'; ?></td></tr>
                </table>
            </div>
        </div>