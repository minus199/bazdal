<?php
/**
 *
 * Created by PhpStorm.
 * User: minus
 * Date: 5/11/14
 * Time: 8:13 PM
 */


class Calculator {
    protected $outcome = NULL; /* Final number */
    protected $op = array(); /* Elements to build the equation */
    protected $equation;
    protected $sign;
    protected $groupsByOperator;
    public $lastValue = null;
    public $lastData = array();

    public function __construct(){}

    public function add(array $data){
        $this->lastValue = ($this->lastValue ? array_sum($data) : 0);
        $this->outcome = (!is_null($this->outcome) ? $this->lastValue + array_sum($data) : array_sum($data));
        $this->setData($data, "+");
        return $this;
    }

    public function subtract(array $data){
        $this->lastValue = ($this->lastValue ? array_sum($data) * -1 : 0);
        $this->outcome = (!is_null($this->outcome) ? $this->lastValue + array_sum($data) * -1 : array_sum($data) * -1);
        $this->setData($data, "-");
        return $this;
    }

    public function multiplie(array $data){
        $this->lastValue = ($this->lastValue ? array_product($data) : 0);
        $this->outcome = (!is_null($this->outcome) ? $this->lastValue + array_product($data) : array_product($data));
        $this->setData($data, "*");
        return $this;
    }

    public function divide(array $data){
        if (!(float)$data[0]){
            throw new Exception("Division by zero not allowed");
        }

        $this->outcome = ($this->outcome ? $data[0] : $this->outcome / (float)$data[0]);

        foreach (array_slice($data, 1, count($data)) as $value){
            if (!$value){
                throw new Exception("Division by zero not allowed");
            }

            $this->outcome /= (float)$value;
        }

        $this->setData($data, "/");
        return $this;
    }

    public function printOutcome(){
        printf("For Total output %o, Elements are %s.",$this->outcome, $this->op);
    }

    public function getEquation(){
        return $this->equation;
    }

    public function getOutcome(){
        return $this->outcome;
    }

    protected function setData($data, $operator){
        /* fallback if array is empty */
        if (empty($this->lastData)){
            $this->equation .= "(" . implode($operator, $data) . ")";

        }

        /* Im making an assumption that the first argument used in each math function will be the last outcome unless its the first function used */
        var_dump($data);
        $a = end($this->lastData);
        $dump = (isset($a) ? end($a['data']) : NULL);
        var_dump($dump);
        echo "\n******\n";
        /* build equation */
//        $this->lastData = (!isset($this->lastData['data']) ? array('data' => array(), 'operator' => '') : $this->lastData);

        //$this->equation .= $this->lastData['operator'] . " (" . implode($operator, $data);


        //$this->equation = implode($operator, $data) . " || " . implode($this->lastData['operator'], $this->lastData['data']);

        /* store last data for next use */
        $this->lastData[] = array('data' => $data, 'operator'=> $operator);
    }

    public function reset(){
        $this->outcome = null;
        $this->op = null;
        $this->equation = null;
        $this->sign = null;
        $groupsByOperator = null;
        return $this;
    }
}
    /* Usage example: */
    $calc = new Calculator();
    /* A. (1 + 5 + 3 + 7) / 2 */
    $currentCalculation = $calc
        ->add(array(1,5,3,7))
        ->multiplie(array($calc->getOutcome(), 2))
        ->add(array($calc->getOutcome(), 5))
        ->getEquation();


    echo PHP_EOL;
    printf("Current inputted calculation was %s.", $currentCalculation);
    echo PHP_EOL;

    /* B. 8 + 4 + (5 X 7) / 4 */

    $currentCalculation = $calc
        ->reset()
        ->add(array(8,4))
        ->multiplie(array(5,7))
//        ->divide(array($calc->getOutcome(), 4))
        ->getEquation();

    echo PHP_EOL;
    printf("Current inputted calculation was %s.",$currentCalculation);
    echo PHP_EOL;
exit;

    /* C. (5 X (9 / (5 + 3))) */

$sumNumbers = array('2', 3, 4.567, '87,900');
    $multiplieNumbers = array(8, 14);
    $divideNumbers = array(13, 5);
    $subNumbers = array(9, 32);

    $output1 = $calc->add($sumNumbers)->subtract($subNumbers)->multiplie($multiplieNumbers);

    var_dump($calc->divide($calc->getOutcome(), 8)->setOutcome());


    /*$output2 = $calc;
    $calc->divide($output1, $output2);
    var_dump($calc->setOutcome());*/


