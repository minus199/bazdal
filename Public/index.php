<html>
    <head>
        <meta charset="UTF-8">
        <title>Translator</title>

        <script src="Public/JS/sonic.js-master/src/sonic.js" type="text/javascript"></script>
        <script src="Public/JS/jQuery.js" type="text/javascript"></script>
        <script src="Public/JS/jquery.form.js" type="text/javascript"></script>
        <script src="Public/JS/JqueryUI-1.10.4/js/jquery-ui-1.10.4.custom.js" type="text/javascript"></script>
        <script src="Public/JS/jquery-upload-file-master/js/jquery.uploadfile.js" type="text/javascript"></script>
        <script src="Public/JS/jquery-visible-master/jquery.visible.js" type="text/javascript"></script>
        <script src="Public/JS/jquery.scrollTo-1.4.12/jquery.scrollTo.min.js" type="text/javascript"></script>

        <link rel="stylesheet" type="text/css" href="Public/CSS/styles.css">
        <link rel="stylesheet" type="text/css" href="Public/CSS/list.css">
        <link rel="stylesheet" type="text/css" href="Public/CSS/inputElements.css">
        <link rel="stylesheet" type="text/css" href="Public/CSS/accordion.css">
        <link rel="stylesheet" type="text/css" href="Public/CSS/tables.css">
        <link rel="stylesheet" type="text/css" href="Public/CSS/stats.css">

        <link rel="stylesheet" type="text/css" href="Public/JS/JqueryUI-1.10.4/development-bundle/themes/ui-lightness/jquery.ui.accordion.css">
        <link href="Public/JS/jquery-upload-file-master/css/uploadfile.css" rel="stylesheet">
    </head>

    <body>

        <?php include_once 'Layouts/HotKeys.php'; ?>
        <?php include_once 'Layouts/StringList.php'; ?>
        <?php include_once 'Layouts/MainTranslationPanel.php'; ?>


        <script src="Public/JS/TranslationPanel/view.js" type="text/javascript"></script>
        <script src="Public/JS/TranslationPanel/strings.js" type="text/javascript"></script>
        <script src="Public/JS/TranslationPanel/stringList.js" type="text/javascript"></script>
        <script src="Public/JS/TranslationPanel/files.js" type="text/javascript"></script>
        <script src="Public/JS/TranslationPanel/spinnerJS.js" type="text/javascript"></script>
        <script type="text/javascript">
            BaseURL = '<?php echo BaseURL;?>';
            PublicURL = '<?php echo PublicURL;?>';
        </script>
    </body>
</html>