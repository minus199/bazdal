    $(document).ready(
        function(){
            $(this).ajaxStart(function (a) {
                window.spin = new spinner();
                window.spin.create();
            }).ajaxStop(function (a) {
                window.spin.destroy();
            });
            initDragAndDrop();
            getXmlFiles();
            getNextString();
            getStringStatistics();
            hotKeys();
        }
    );

    $("#mainPanel").accordion(
        {
            heightStyle: "content",
            autoHeight: false,
            clearStyle: true,
            active: false,
            collapsible: true,
            event: 'click'
        }
    );

    $("#downloadLinks").click(
        function(){
            if ($(this).attr('href') == '' || $(this).attr('href') != undefined){
                return;
            }

            createDownloadLinks();
        }
    );

    $("#keyMapTrigger").click(
        function() {
            $("#keyMap").toggle("slide" , 500);
        }
    );

    function initDragAndDrop(){
        $("#fileuploader").uploadFile(
            {
                url:BaseURL + "translationHelperAjax/addNewXMLFile",
                fileName:"tmpFile",
                onSuccess: function(data) {onUploadComplete()}
            }
        );
    }

    function hotKeys(){
        var keys = {};

        $(document).keydown(function (e) {
            keys[e.which] = true;

            if (!isNaN(parseInt(String.fromCharCode(e.which)))){
                var keyValue = parseInt(String.fromCharCode(e.which));
            }

            verifyKeys(keys, keyValue);
        });

        $(document).keyup(function (e) {
            delete keys[e.which];
            verifyKeys(keys);
        });
    }

    function verifyKeys(keys, keyValue) {
        if (keys[27] === true){
            $("#previewStringContent").hide().text('');
        }

        if (!isNaN(parseFloat(keyValue)) && isFinite(keyValue)) {
            $("#ui-accordion-mainPanel-header-" + (keyValue - 1)).click();
            return;
        }

        /* 16 -> shift, 83 -> s, 27 -> esc */
        if (keys[17] === true && keys[13] === true){
            if ($("#targetString").is(":focus") && $("#targetString").val() != ''){
                /* Ctrl+Enter -> should submit current translation and fetch next string */
                submitCurrentTranslation();
                generateStringsList();
            }
        }

        if ((keys[17] === true && keys[38] === true && parseInt($("#listHolder").css('height')) <= 40) || (keys[17] === true && keys[40] === true && parseInt($("#listHolder").css('height')) > 40)){
            /* ctrl+up */
            setListView();
        }

        /* List up and down arrow nav */
        if (Object.keys(keys).length === 1 && keys[40] === true){
            /* Down */
            if ($(".ui-selected").next().length > 0){
                $(".ui-selected").removeClass("ui-selected").next().addClass(('ui-selected'));
                $("#listHolder").stop().scrollTo( '.ui-selected', 300, {offset: function() { return {top:-200}; }});
            }
        }

        if (Object.keys(keys).length === 1 && keys[38] === true ){
            /* Up */
            if ($(".ui-selected").prev().length > 0){
                $(".ui-selected").removeClass("ui-selected").prev().addClass(('ui-selected'));
                $("#listHolder").stop().scrollTo( '.ui-selected', 300, {offset: function() { return {top:-200}; }});
            }
        }
        /* END List up and down arrow nav */

        if (keys[16] === true && keys[72] === true){
            /* shift+h -> show key map */
            var opacity = $( "#keyMapTrigger" ).css('opacity') < 1 ? 1 : 0.4;
            $( "#keyMapTrigger" ).animate({ opacity: opacity });

            $("#keyMap").toggle("slide");
        }
    }

    function toggleLabels(appendElement, msg){
        var currentDiv = $("<div/>");
        currentDiv.text(msg).show().fadeOut( 4000 );
        $(appendElement).append(currentDiv);
        currentDiv.show().fadeOut( 4000);
        setTimeout(function() { currentDiv.remove(); }, 4000);
    }

    function onUploadComplete(){
        $("#transFile").toggleClass('uploaded');
        $("#srcFile").toggleClass('uploaded');

        setTimeout(
            function(){
                $("#transFile").removeClass('uploaded');
                $("#srcFile").removeClass('uploaded');
            },
            5000
        );
    }




