/**
 * Created by minus on 5/28/14.
 */

$("#contentTitle").click(function() { setListView(); } );

$("#goToCurrentString").click(function(){
    if ($('.ui-selected').visible()) return;


    var $holder = $("#listHolder");
    $holder.animate({scrollTop: $('.ui-selected').offset().top - ($holder.offset().top + 250)});
});

$("#searchOffset").keyup(function(){searchOffset();})

/* Scrolls */
/* Increade/ Decrease Scroll speed */
$('#listHolder').bind('mousewheel', function(event){
    if ((event.originalEvent.wheelDelta) > 0){
        var speed = event.originalEvent.wheelDelta/120 * 20 * 3;
        $('#listHolder').scrollTop($('#listHolder').scrollTop() - speed);
    } else {
        /* Down */
        var speed = event.originalEvent.wheelDelta/120 * -20 * 3;
        $('#listHolder').scrollTop($('#listHolder').scrollTop() + speed);
    }
});

$("#scrollStrListDown").hover(function(){
    $("#listHolder").animate({ scrollTop: $('#listHolder')[0].scrollHeight}, 20000);
},function(){
    $("#listHolder").stop();
});

$("#scrollStrListUp").hover(function(){
    $("#listHolder").animate({ scrollTop: $('#listHolder').offset().top - 200}, 20000);
},function(){
    $("#listHolder").stop();
});
/* Scrolls */

/* Make the list holder jQueryUI selectable */
function makeListSelectable(){
    $("#strList > tbody").selectable({
        // Don't allow individual table cell selection.
        filter: ":not(td)",
        selected: function(){ onListSelect(); setListView(); }
    });
}

function setListView(){
    var $holder = $("#listHolder");
    $holder.animate({height: (parseInt($holder.css('height')) > 40 ? '40px' : '75%')});

    $holder.animate({scrollTop: $('.ui-selected').offset().top - ($holder.offset().top + 250)});

    $(".listValues-target, .listValues-source").hover(
        function(){
            var currentPreview = $(this).text();
            var currentPosition = ($(this).position());
            if(currentPreview != ''){
                $("#previewStringContent").show().text(currentPreview).position(currentPosition);
            }

        },
        function(){ /*$("#previewStringContent").hide().text('');*/ }
    );
}

function onListSelect() {
    var currentOffset = $(".ui-selected .listValues-offset").text();
    var currentSource = ($(".ui-selected .listValues-source").text() != '--Empty--' ? $(".ui-selected .listValues-source").text() : '');
    var currentTarget = ($(".ui-selected .listValues-target").text() != '--Empty--' ? $(".ui-selected .listValues-target").text() : '');

    populateStringData(currentOffset, currentSource, currentTarget);
}

function searchOffset(){
    $("#strList").children().remove();

    var ajaxParams = {
        url: BaseURL + "translationHelperAjax/getOffsetMatches",
        type: 'POST',
        dataType: "json",
        data: {'term': $("#searchOffset").val()},

        success: function(data){
            var $holder = $("#strList");
            window.data = data;
            $.each(data,
                function (index, stringData){
                    generateStringListLiElement($holder, stringData);
                }
            );
        }
    }

    $.ajax(ajaxParams);
}
