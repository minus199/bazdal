    function getXmlFiles(){
        $.ajax(
            {
                url: BaseURL + "translationHelperAjax/getXMLFolder",
                type: "POST",
                dataType: "json",
                success: function(data){
                    window.files = data;
                    var selects = $("#stringFiles select");

                    $.each(selects, function ($index, select) {
                            var $currentSelect = $(select);
                            generateDropDown($currentSelect, data);
                            
                            /* On files drop down change, save preferred file settings */
                            $currentSelect.change( function (){ setPreferedFile($(this).attr('id'), $(this).val()); } );

                            var currentID = $currentSelect.attr('id');
                            var $currentSelectChildren = $("#" + currentID+ " > option");
                            $.each($currentSelectChildren, function ($index, $option) {selectPreferedFiles($option)});

                            try {
                                $("#" + currentID + " > #" + getPreferredFileID(currentID)).attr('selected', true);
                            } catch(err){
                                toggleLabels("label[for='stringFiles']", currentID + ' was not selected yet.');
                            }
                        }
                    );
                },
                complete: function () {verifySelectedFiles();}
            }
        );
    }

    function generateDropDown(currentSelect, data){
        /* Generate files drop down */
        $(currentSelect).children().remove();
        var folderContent = data.folderContent;
        var $newOption = $('<option />', { text: 'None', value: 'none' } );

        currentSelect.append($newOption);
        $.each(folderContent, function($k, $v){
            var $newOption = $('<option />',
                {
                    text: $v,
                    id: $v.split(".")[0],
                    value: $v
                }
            );

            currentSelect.append($newOption);
        });
    }

    function setPreferedFile(id, file){
        var options = {
            fieldID: id,
            selectedFile: file
        };

        $.ajax(
            {
                url: BaseURL + "translationHelperAjax/setXMLFiles",
                type: "POST",
                dataType: "json",
                data: options,
                success: function(data){
                    //getXmlFiles();
                    var file = (data.file == "" || data.file == undefined ? 'not selected' : data.file);
                    toggleLabels("label[for='stringFiles']", "File for " + id + " is now " + file);
                    copyTranslatedStrings(id, data.file);
               }
            }
        );
    }

    function getPreferredFileID(id){
        switch (id){
            case 'srcFile':
                var currentID = window.files.config['file.dst.strings.src'];
                currentID = currentID.split('.')[0];
                break;
            case 'transFile':
                var currentID = window.files.config['file.dst.strings.target'];
                currentID = currentID.split('.')[0];
                break;
        }

        return currentID
    }

    function copyTranslatedStrings(id, file){
        if (window.files.config['file.dst.strings.source'] != ''){
            $.ajax(
                {
                    url: BaseURL + "translationHelperAjax/copyTranslatedStrings",
                    type: "POST",
                    success: function(data){
                        verifySelectedFiles();
                    }
                }
            );
        }
    }

    function selectPreferedFiles(option) {
        var $option = $(option);

        switch ($option.attr('id')){
            case 'Source':
                var currentID = window.files.config['file.dst.strings.src'];
                break;
            case 'Target':
                var currentID = window.files.config['file.dst.strings.target'];
                break;
            default:
                var currentID = 'none';
        }

        $("#" + currentID).attr('selected', true);
    }

    function verifySelectedFiles(){
        $.ajax(
            {
                url: BaseURL + "translationHelperAjax/getXMLFolder",
                type: "POST",
                dataType: "json",
                success: function(data){
                    window.files = data;
                    var srcFile = window.files.config['file.dst.strings.src'];
                    var targetFile = window.files.config['file.dst.strings.target'];

                    if (targetFile == ''){
                        $("label[for='stringFiles']").text('If target is not set, file will be generated based on source file.').show().fadeOut( 15000 );
                    }

                    if (srcFile == ''){
                        /* one of the files is not selected, dont show translation panel */
                        $("#ui-accordion-accordion-header-1").hide();
                        $("label[for='stringFiles']").text('Select at least source file before starting.').show().fadeOut( 15000 );
                        return false;
                    }

                    $("#ui-accordion-accordion-header-1").show();
                    return true;
                }
            }
        );
    }

    function createDownloadLinks(){
        $.ajax(
            {
                url: BaseURL + "translationHelperAjax/getXMLFolder",
                type: "POST",
                dataType: "json",
                success: function(data){
                    var $link = $("#downloadLinks");
                    var targetFile = data.config['file.dst.strings.target'];
                    $link.attr('href', BaseURL + "Download/xml/?fid=" + targetFile)
                    $link.text('Download Target File');
                }
            }
        );
    }