function getNextString(){
    var ajaxParams = {
        url: BaseURL + "translationHelperAjax/getNextString",
        type: 'POST', dataType: "json",

        success: function(data){
            populateStringData(data.offset, data.value, '');
            generateStringsList();

        }
    }

    $.ajax(ajaxParams);
}

function populateStringData(offset, value, translation) {
    $('#currentName').val(offset);
    $('#sourceString').val(value);
    $("#targetString").val(translation);
    $("#ui-accordion-mainPanel-header-1").show();
    $("#targetString").focus();

}

function submitCurrentTranslation(){
    var stringAttr = {
        offset: $('#currentName').val(),
        translation: $('#targetString').val()
    };

    var ajaxParams = {
        url: BaseURL + "translationHelperAjax/translate",
        type: 'POST', data: stringAttr, dataType: "json",
        success: function(data){ getStringStatistics(); getNextString(); generateStringsList();  }
    }

    $.ajax(ajaxParams);
}

function generateStringsList(){
    $("#strList").children().remove();

    var ajaxParams = {
        url: BaseURL + "translationHelperAjax/getStringsList",
        type: 'POST', dataType: "json",

        success: function(data){
            var $holder = $("#strList");
            $.each(data,
                function (index, stringData){
                    generateStringListLiElement($holder, stringData);
                    makeListSelectable();
                }
            );
        }
    }

    $.ajax(ajaxParams);
}

function generateStringListLiElement($holder, stringData){
    if(stringData.offset === undefined){
        $.each(stringData,
            function(index, subData){
                generateRowCells($holder, subData, $("<tr />"));
            }
        );

        return;
    }

    generateRowCells($holder, stringData, $("<tr />", { class: 'ui-selected' }).css('font-weight', 'bold'));
}

function generateRowCells($holder, stringData, $trElement){
    var translatedIndication = $("<div/>");

    translatedIndication.addClass(stringData.targetValue ? 'translated' : 'untranslated');
    $trElement.append($("<td/>").append(translatedIndication.clone()));
    $trElement.addClass(stringData.targetValue ? 'translated1' : 'untranslated1');

    var idPrefix = stringData.offset + "_";

    $trElement.append($("<td/>", {class: "listValues-offset code fixwidth", id: idPrefix + "offset", text: stringData.offset}));
    $trElement.append($("<td/>", {class: "listValues-source fixwidth", id: idPrefix + "source", text: stringData.sourceValue}));
    $trElement.append($("<td/>", {class: "listValues-target fixwidth", id: idPrefix + "target", text: stringData.targetValue}).css('width', '100'));

    $trElement.children().css('padding', '5px');
    $holder.append($trElement);
}

function isHTML(str) {
    var a = document.createElement('div');
    a.innerHTML = str;
    for (var c = a.childNodes, i = c.length; i--; ) {
        if (c[i].nodeType == 1) return true;
    }
    return false;
}

function getStringStatistics(){
    var ajaxParams = {
        url: BaseURL + "Statistics/getStringStatistics",
        type: 'GET', dataType: "json",

        success: function(data){
            populateStats(data)
        }
    }

    $.ajax(ajaxParams);
}

function populateStats(data){
    var $holder = $("#stringStatistics");
    $holder.children().remove();

    var $total = $("<span />", {class: 'statistics-total', id: 'totalCount', text: data.total });
    $holder.append($("<span />", {text: 'Total: '})).append($total);

    var $translatedAbs = $("<span />", {class: 'statistics-abs', id: 'translatedAbs', text: data.translated.abs, ratio: data.translated.ratio + "%"});
    $holder.append($("<span />", {class: 'statistics-title', text: 'Translated: '})).append($translatedAbs);

    var $unTranslatedAbs = $("<span />", {class: 'statistics-abs', id: 'untranslatedAbs', text: data.unTranslated.abs, ratio: data.unTranslated.ratio + "%"});
    $holder.append($("<span />", {class: 'statistics-title', text: 'Untranslated: '})).append($unTranslatedAbs);
}
