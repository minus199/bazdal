/**
 * Created by minus on 5/9/14.
 */
var spinner = function(){


    this.data = {
        width: 200,
        height: 200,
        padding: 20,

        stepsPerFrame: 1,
        trailLength: 1,
        pointDistance: .04,
        fps: 20,

        fillColor: '#05E2FF',

        step: function(point, index) {
            this._.beginPath();
            this._.moveTo(point.x, point.y);
            this._.arc(point.x, point.y, index * 7, 0, Math.PI*2, false);
            this._.closePath();
            this._.fill();
        },

        path: [
            ['arc', 90, 90, 90, 00, 360]
        ]

    };

    this.create = function(){
        var parentElement = $("body");
        this.container = $("<div/>", {id: 'spinnerContainer'});

        var loader = new Sonic(this.data);
        var canvas = loader.canvas;

        this.container.append(canvas);
        parentElement.append(this.container);
        window.container = this.container;

        loader.play();
    }

    this.destroy = function(){
        window.container.remove();
    }
};