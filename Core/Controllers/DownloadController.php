<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/5/14
 * Time: 7:07 PM
 */

namespace Controllers;


class DownloadController {
    function xmlAction(){
        $file = "Public/XML/" . $_GET['fid'];

        if ($fd = fopen ($file, "r")) {
            $fileSize = filesize($file);
            $pathInfo = pathinfo($file);
            $ext = strtolower($pathInfo["extension"]);
            switch ($ext) {
                case "xml":
                    //header("Content-type: application/xml");
                    header("Content-Disposition: attachment; filename=\"" . $pathInfo["basename"]."\""); // use 'attachment' to force a download
                    break;
                default;
                    header("Content-type: application/octet-stream");
                    header("Content-Disposition: filename=\"" . $pathInfo["basename"] . "\"");
                    break;
            }

            header("Content-length: $fileSize");
            header("Cache-control: private"); //use this to open files directly

            while(!feof($fd)) {
                $buffer = fread($fd, 2048);
                echo $buffer;
            }
        }
        fclose ($fd);
        exit;
    }
} 