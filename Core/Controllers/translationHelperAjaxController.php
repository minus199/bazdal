<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 4/27/14
 * Time: 7:50 PM
 */

namespace Controllers;


use Core\stringsModel;

class translationHelperAjaxController extends stringsModel {
    public function __construct(){
        parent::__construct();
    }

    /* Files Handling */
    public function addNewXMLFileAction(){
        global $config;

        $basePath = $config->offsetGet('files', 'file.dst.strings.dir') . DIRECTORY_SEPARATOR;
        $currentFileName = $_FILES["tmpFile"]["name"];
        $currentFile = $_FILES["tmpFile"]["tmp_name"];

        libxml_use_internal_errors(true);
        /* File is invalid XML */
        if (!simplexml_load_string(file_get_contents($currentFile))){
            //echo json_encode(array('error' => 'Current selected file is not valid xml. Fuck off'));
            echo json_encode(array('error' => 'file is not a valid XML.'));
            exit;
        }

        /* Get file name without extension */
        list($nameWithoutExtension) = explode(".", $currentFileName);
        $currentFileName =  $basePath . $nameWithoutExtension . '.xml';
        $msg = array('attention' => 'File ' . $currentFileName . ' was saved.');

        if (file_exists($currentFileName)){
            $currentFileName =  $basePath . $nameWithoutExtension . uniqid() . '.xml';
            $msg = array('attention' => 'File was created under the name ' . $currentFileName);
        }

        move_uploaded_file($currentFile, $currentFileName);
        chmod($currentFileName, 0777);

        echo json_encode($msg);
    }

    public function getXMLFolderAction(){
        global $config;
        $output = array();
        $configFiles = $config->offsetGet('files');
        array_walk($configFiles, function (&$file) {$file = basename($file);});

        $file = dirname(dirname(dirname(__FILE__))) . "/Public/XML";
        $folderFiles = scandir($file);
        foreach ($folderFiles as $index => $xmlFile){
            if ($xmlFile != '.' && $xmlFile != '..'){
                $output[$index] = $xmlFile;
            }
        }

        echo json_encode(array('folderContent' => $output, 'config' => $configFiles));
    }

    public function setXMLFilesAction(){
        global $config;

        $files = $config->offsetGet('files');
        $currentSelectID = $this->_post('fieldID');
        $selectedFile = ($this->_post('selectedFile') == 'none' ? '' : $this->_post('selectedFile'));
        $basePath = $files['file.dst.strings.dir'] . DIRECTORY_SEPARATOR;

        /* Config offsets by select menu id */
        $configOffsets = array('srcFile' => 'file.dst.strings.src', 'transFile' => 'file.dst.strings.target');

        $isSelectedFileExists = file_exists($basePath . $selectedFile);
        if ((!$currentSelectID || !$isSelectedFileExists) && $selectedFile != ''){
            echo json_encode(array('error' => 'Invalid select id or Invalid File'));
            return;
        }

        $files[$configOffsets[$currentSelectID]] = (empty($selectedFile) ? $selectedFile : $basePath . $selectedFile);

        $config->offsetSet('files', $files);
        $config->save();

        echo json_encode(array('file' => $selectedFile, 'id' => $currentSelectID));
    }

    /* Strings Handling */
    public function getNextStringAction(){
        echo json_encode($this->getFirstStringWithoutTranslation());
    }

    public function getStringsListAction(){
        echo json_encode($this->getStringsList());
    }

    public function copyTranslatedStringsAction(){
        global $config;
        $files = $config->offsetGet('files');
        if (!empty($files['file.dst.strings.src']) && !empty($files['file.dst.strings.target'])){
            $this->copyAlreadyTranslated();
        } else {
            json_encode(array('error' => 'Strings were not translated, internal error'));
        }
    }

    public function translateAction(){
        $currentOffset = $this->_post('offset');
        $currentTranslation = $this->_post('translation');

        if (!$currentOffset || !$currentTranslation){
            /* Display error message, return first string with no translation */
            echo json_encode(array('error' => 'insufficient data.'));
            exit;
        }

        $this->targets[$currentOffset]['value'] = $currentTranslation;
        $this->targets[$currentOffset]['untranslated'] = 0;

        if ($outputAsXML = $this->array2XML($this->targets)){
            global $config;
            $files = $config->offsetGet('files');
            $file = $files['file.dst.strings.target'];

            if (file_put_contents($file, $outputAsXML)){
                echo json_encode(array('errors' => array(), 'status' => 'ok', 'message' => 'output created'));
                return;
            }
        }

        echo json_encode(array('errors' => array('internal'), 'status' => 'failed', 'message' => 'output was not created'));
    }

    /**
     * @deprecated
     */
    public function getNextStringActionDONTUSE(){
        /* DEBUGGING */
        //$this->currentKey = 'clearCallLogConfirmation_title'; // lets say i get this from the view
        /* DEBUGGING */
        $this->getDefaultString();
        if (!$this->currentKey){
            //$output = json_encode(array('error' => 'Insufficient data'));
            $this->getDefaultString();
        } else {
            /* Verify current offset(name) exists in sources */
            $isSrcExists = (array_search($this->currentKey, $this->sourcesKeys) !== false ? array_search($this->currentKey, $this->sourcesKeys) : NULL);
            /* Verify current offset(name) exists in targets */
            $isTargetExists = (array_search($this->currentKey, $this->targetsKeys) !== false ? array_search($this->currentKey, $this->targetsKeys) : NULL);
            /* Verify current offset(name) has translation*/
            $currentTranslation = (isset($this->stringOutputTarget[$this->currentKey]) ? $this->stringOutputTarget[$this->currentKey] : NULL);


            /*!$isSrcExists && !$isTargetExists && !$currentTranslation*/
            if (!$isSrcExists){
                $output = array('error' => 'Invalid string');
            } elseif ($isSrcExists && $isTargetExists) {
                /* return next string with no translation */
                $this->validateNextString($this->currentKey); /* this is the next string, do this until no translation is found */
                $output = array('error' => array(), 'key' => $this->currentKey, 'src' => $this->sources[$this->currentKey]);
            } elseif ($isSrcExists && !$isTargetExists) {
                /* Check if request has translation inside, if it does, save translation and return next string with no translation */
                $this->validateNextString($this->currentKey); /* this is the next string, do this until no translation is found */
                $output = array('error' => array(), 'key' => $this->currentKey, 'src' => $this->sources[$this->currentKey]);
            } else {
                $output = array('error');
            }
        }

        echo json_encode($output);
    }

    /**@deprecated */
    protected function getDefaultString($skip = false){
        if ($skip) return;
        $currentData = array_filter($_POST);
        if (empty($currentData)){
            /** TODO: find the first offset of a name with no translation in it
             */
            $currentName = $this->sourcesKeys[0];
            $currentValue = (isset($this->sources[$currentName]) ? $this->sources[$currentName] : "");
            $currentTranslation = (isset($this->targets[$currentName]) ? $this->targets[$currentName] : "");
            echo json_encode(array('name' => $currentName, 'srcValue'=>$currentValue, 'translation' => $currentTranslation ));
            exit;

        }

        $this->currentKey = $this->_post('currentName');
    }

    public function getOffsetMatchesAction(){
        $searchWord = $this->_post('term');

        if ($searchWord == ''){
            $this->getStringsListAction();
            return;
        }

        $output = array();
        foreach(array_keys($this->sources) as $source){
            if (is_numeric(strpos($source, $searchWord))){
                $output['before'][] = $data = array('offset' => $source, 'sourceValue' => $this->sources[$source]['value'], 'targetValue' => $this->targets[$source]['value']);
            }
        }

        echo json_encode($output);
    }
}