<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/27/14
 * Time: 10:22 PM
 */

namespace Controllers;


use Statistics\StringStatistics;

class StatisticsController extends StringStatistics{

    public function __construct(){
        parent::__construct();
    }

    public function getStringStatisticsAction(){
        echo json_encode($this->queryData());
    }
} 