<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 4/10/14
 * Time: 7:28 PM
 */

namespace Controllers;


use Statistics\StringStatistics;

class IndexController extends StringStatistics{
    public function __construct(){

    }

    public function index(){
        ob_start();
        //header('Content-type: text/html; charset=utf-8');
        include('Public/index.php');
        echo ob_get_clean();
    }

} 