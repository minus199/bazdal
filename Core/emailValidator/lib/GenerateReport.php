<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/14/14
 */

class GenerateReport extends emailValidate{
    public function fullyValidateList(){
        if (!$this->emailList){
            echo "List is empty, no test are needed";
            exit;
        }

        $report = array();
        foreach($this->emailList as $emailAddress){
            $report[] = $this->fullValidation($emailAddress);
        }

        return $report;
    }

    public function fullValidation($email){
        $report = array();

        /* The email is in valid format */
        $report[$email]['Validation'] = ($this->isEmailValid($email) ? 'Valid' : 'Invalid');

        /* The email does **not** come from prohibited domain */
        $report[$email]['From a prohibited domain'] = ($this->isProhibitedDomain($email) ? 'Yes: ' . $this->isProhibitedDomain($email) : 'No');

        /* The email does **not** contain any words that are specified locally */
        $report[$email]['Illegal keywords'] = $this->isContainIllegalKeyWord($email) ? $this->isContainIllegalKeyWord($email) : 'None';

        /* Checks if email contains any other of the following: all characters except letters, digits and !#$%&'*+-/=?^_`{|}~@.[] */
        /* If not, considered sanitized from invalid chars */
        $report[$email]['Sanitized'] = ($this->isEmailSanitized($email) ? 'true' : 'false');

        /* Verify the email does not contain any pattern of chars from the list, for example: any alphanumeric char that appears more than once */
        /* The method will return true is matched to any illegal patterns, so this statement is negated */
        $report[$email]['Illegal Patterns'] = ($this->isEmailContainsPatternChars($email) ? $this->isEmailContainsPatternChars($email) : 'None');

        return $report;
    }
} 