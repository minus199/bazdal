<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/13/14
 */

class emailValidate {
    protected $emailList;
    protected $locallyKnownDomains;
    protected $prohibitedDomainsList;
    protected $illegalKeywordsList;
    protected $prohibitedPatterns;

    public function __construct(){
        /* Data Files */
        $emailListFile = 'data/emailList.txt';
        $prohibitedDomainsList = 'data/prohibitedDomainsList.txt';
        $illegalKeywordsList = 'data/illegalKeywordsList.txt';
        $prohibitedPatterns = 'data/prohibitedPatterns.txt';

        /* Import Data */
        $this->emailList = $this->loadList($emailListFile);
        $this->prohibitedDomainsList = $this->loadList($prohibitedDomainsList);
        $this->illegalKeyWords = $this->loadList($illegalKeywordsList);
        $this->prohibitedPatterns = $this->loadList($prohibitedPatterns);
    }

    protected function loadList($listFile){
        if (file_exists($listFile)){

        } elseif(file_exists("../" . $listFile)){
            $listFile = '../' . $listFile;
        } else {
            return null;
        }

        return $listFile ? array_filter(explode("\n", file_get_contents($listFile))) : NULL;
    }

    public function isEmailValid($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) ? true : false;
    }

    public function isEmailSanitized($email){
        return (!filter_var($email, FILTER_SANITIZE_EMAIL) ? true : false);
    }

    public function isEmailContainsPatternChars($email){
        if (!$this->prohibitedPatterns){
            return false;
        }

        $found = array();
        foreach ($this->prohibitedPatterns as $pattern){
            if (preg_match($pattern, $email, $matches)){
                $found[$email][] = $matches;
            }
        }

        return (!empty($found) ? $found : false);
    }

    public function isProhibitedDomain($email){
        $emailDomain = end(explode("@", $email));
        return (in_array($emailDomain, $this->prohibitedDomainsList) ? $emailDomain : false);
    }

    public function isContainIllegalKeyWord($email){
        if (!$this->illegalKeyWords){
           return false;
        }

        $found = array();
        foreach ($this->illegalKeyWords as $keyWord){
            if (is_numeric(strpos($email, $keyWord))){
                $found[] = $keyWord;
            }
        }

        return (!empty($found) ? $found : false);
    }

    public function mailboxConnect($email){
        list ($localPart, $domain) = explode("@", $email);
        return (fsockopen('smtp.' . $domain, 25) ? true : false );
    }
}