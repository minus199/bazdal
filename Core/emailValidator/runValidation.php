<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/14/14
 */

require_once 'lib/emailValidate.php';
require_once 'lib/GenerateReport.php';

$email = isset($argv[1]) ? $argv[1] : NULL;
$validator = new emailValidate();

/* Validate specific email address when filters are specified */
if ($email && isset($argv[2])){
    $output = array();
    for ($i = 2; $i < count($argv); $i++){
        switch ($argv[$i]){
            case 'valid':
                $output['valid'] = ($validator->isEmailValid($email) ? 'true' : 'false');
                break;
            case 'sanitized':
                $output['sanitized'] = ($validator->isEmailSanitized($email) ? 'true' : 'false');
                break;
            case 'pattern':
                $output['patterns'] = ($validator->isEmailContainsPatternChars($email) ? $validator->isEmailContainsPatternChars($email) : 'None');
                break;
            case 'domain':
                $output['Prohibited Domains'] = ($validator->isProhibitedDomain($email) ? $validator->isProhibitedDomain($email) : 'None');
                break;
            case 'keyword':
                $output['Illegal keywords'] = ($validator->isContainIllegalKeyWord($email) ? $validator->isContainIllegalKeyWord($email) : 'None');
                break;
        }
    }
    print_r($output);
    exit;
}

$generateReport = new GenerateReport();
/* Run full validation on a specific email address */
if ($email){
    print_r($generateReport->fullValidation($email));
    exit;
}

/* Run on the entire list in the file and validate each */
print_r($generateReport->fullyValidateList());
