<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/27/14
 * Time: 9:24 PM
 */

namespace Statistics;

use Core\stringsModel;

class StringStatistics extends stringsModel{
    public function __construct(){
        parent::__construct();
    }

    protected function getTotalNumberOfString(){
        return count(array_keys($this->sources));
    }

    protected function getTotalUntranslatedStringAbs(){
        $count = 0;

        foreach($this->targets as $targetString){
            $count = $targetString['untranslated'] == (int)1 ? $count += 1 : $count;
        }

        return $count;
    }

    protected function getTotalTranslatedStringAbs(){
        $count = 0;

        foreach($this->targets as $targetString){
            $count = $targetString['untranslated'] == (int)0 ? $count += 1 : $count;
        }

        return $count;
    }

    protected function getTotalUntranslatedStringRatio(){
        return round($this->getTotalUntranslatedStringAbs() / $this->getTotalNumberOfString() * 100, 2);
    }

    protected function getTotalTranslatedStringRatio(){
        return round($this->getTotalTranslatedStringAbs() / $this->getTotalNumberOfString() * 100, 2);
    }

    protected function queryData(){
        return array(
            'total' => $this->getTotalNumberOfString(),
            'unTranslated' => array(
                'ratio' => $this->getTotalUntranslatedStringRatio(),
                'abs' => $this->getTotalUntranslatedStringAbs()
            ),
            'translated' => array(
                'ratio' => $this->getTotalTranslatedStringRatio(),
                'abs' => $this->getTotalTranslatedStringAbs()
            )
        );
    }
}