<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/22/14
 * Time: 7:37 PM
 */

namespace Core\Bootstrap;

class IBootStrap {

    protected $classes = array();
    protected $config = array();
    protected $controllers = array();
    protected $layouts = array();

    protected $core = array();

    protected static $instance = null;

    public static function getInstance(){
        if(self::$instance === null){
            self::$instance = new IBootStrap();
        }

        return self::$instance;
    }

    private function __construct(){



        $this->scanDirectory('Core');

        foreach($this->core as $type => $subClass){
            $this->scanDirectory($type);
        }
    }

    protected function scanDirectory($item, $extension = null){
        $current = array();

        if ($item !== 'Core'){
            $currentSubDir = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . $item;

            foreach(scandir($currentSubDir) as $subItem){
                $isScanable = ($subItem != '.') && ($subItem != '..') && (pathinfo($subItem, PATHINFO_EXTENSION) == 'php');
                if ($isScanable){
                    $current[] = $subItem;
                }
            }

            $this->core[ucfirst($item)] = $current;
            return $this->core[ucfirst($item)];
        }

        foreach(scandir($item) as $subItem){
            $currentSubDir = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . $subItem;
            $isScanable = is_dir($currentSubDir) && ($subItem != '.') && ($subItem != '..');
            if ($isScanable){
                $this->core[$subItem] = array();
            }
        }

        return $this->core;
    }

    public function getType($class, $onlyType = false){
        $lastPos = strrpos($class, '\\');
        if ($lastPos !== false) {
            $folder = substr($class, 0, $lastPos);
            $className = substr($class, $lastPos + 1);
            foreach ($this->core as $type => $files){
                if(in_array($className . '.php', $files)){
                    return $onlyType ? $type : array('type' => $type, 'class' => $class);
                }
            }
        } else {
            /* Root folder?? */
        }



        return false;
    }

    private function __clone(){}

    private function __wakeup(){}
}