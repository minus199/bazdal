<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/22/14
 * Time: 7:49 PM
 */

namespace Core\Bootstrap;

class ControllersBootstrap extends IBootStrap implements BootstrapInterface{

    protected $controllerName = null;
    protected $action = null;
    protected $params = array();

    public function __construct(){

    }

    public function extractBuildData(){
        global $config;
        /* Extract Controller Data (Name, Method and Args) */
        $requestData = str_replace($config->offsetGet('env', 'base_path'), "",$_SERVER["REQUEST_URI"]);

        /* array_values in order to re-index array index */
        $controllerData = array_values(array_filter(explode("/", $requestData)));

        switch (count($controllerData)){
            case 0:
                /* Home page */
                $controllerName = "IndexController";
                $action = 'index';
                $params = null;
                break;
            case 1:
                /* Only Controller */
                $controllerName = $controllerData[0] . "Controller";
                $action = NULL;
                $params = NULL;
                break;
            case 2:
                /* Controller and method call, no arguments */
                $controllerName = $controllerData[0] . "Controller";
                $action = $controllerData[1] . "Action";
                $params = null;
                break;
            default:
                /* Controller, method call and arguments */
                $controllerName = $controllerData[0] . "Controller";
                $action = $controllerData[1] . "Action";
                $params = array_slice($controllerData,2);
                break;
        }

        $this->controllerName = $controllerName;
        $this->action = $action;
        $this->params = $params;

        return $this;
    }

    public function isExists(){
        try {
            if (!file_exists(ControllersFolder . $this->controllerName . ".php")){
                throw new \Exception('File not found. aborting.');
            }

            if (!is_readable(ControllersFolder . $this->controllerName . ".php")){
                throw new \Exception("file not readable.");
            }
        }

        catch (\Exception $exc) {
            // Log error here
            $this->controllerName = "NotFoundController";
            $this->action = null;
            $this->params = null;
        }

        return $this;
    }

    public function instantiate(){
        spl_autoload_register('__autoload');

        /* Try to load class */

    /*use Controllers\translationHelperAjaxController;

        new translationHelperAjaxController()*/

        $controllerName = "Controllers\\" . $this->controllerName;
        $controller = new $controllerName();

        if ($this->action && is_callable(array($controllerName, $this->action))){

            $action = $this->action;
            if (isset($params)){

                $controller->$action($params);
            } else {
                $controller->$action();
            }

            return true;
        }

        /* Class/Controller should have been loaded by now */
    }

    /**
     * @param null $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param null $controllerName
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;
    }

    /**
     * @return null
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }


}