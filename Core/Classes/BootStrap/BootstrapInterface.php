<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/22/14
 * Time: 8:04 PM
 */

namespace Core\Bootstrap;


interface BootstrapInterface {
    public function extractBuildData();

    public function isExists();

    public function instantiate();
} 