<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 5/10/14
 * Time: 5:11 AM
 */

namespace Core;


class filesModel {
    protected function addAndValidateXML(){
        global $config;

        $basePath = $config->offsetGet('files', 'file.dst.strings.dir') . DIRECTORY_SEPARATOR;
        $currentFileName = $_FILES["tmpFile"]["name"];
        $currentFile = $_FILES["tmpFile"]["tmp_name"];

        libxml_use_internal_errors(true);
        /* File is invalid XML */
        if (!simplexml_load_string(file_get_contents($currentFile))){
            //echo json_encode(array('error' => 'Current selected file is not valid xml. Fuck off'));
            echo json_encode(array('error' => 'file is not a valid XML.'));
            exit;
        }

        /* Get file name without extension */
        list($nameWithoutExtension) = explode(".", $currentFileName);
        $currentFileName =  $basePath . $nameWithoutExtension . '.xml';
        $msg = array('attention' => 'File ' . $currentFileName . ' was saved.');

        if (file_exists($currentFileName)){
            $currentFileName =  $basePath . $nameWithoutExtension . uniqid() . '.xml';
            $msg = array('attention' => 'File was created under the name ' . $currentFileName);
        }

        move_uploaded_file($currentFile, $currentFileName);
        chmod($currentFileName, 0777);

        echo json_encode($msg);
    }

    public function createNewTargetXmlFile(){
        global $config;

        if (!file_exists($config->offsetGet('files', 'file.dst.strings.target'))){

        }

    }
}