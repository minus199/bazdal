<?php


    require_once 'Config.php';
    global $configFile;
    $iniFile = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'config.ini';

    $config = new \Config();
    $config->addIni($iniFile);

    require_once 'Consts.php';
    require_once 'ErrorReporting.php';


    require_once 'BootStrap' . DIRECTORY_SEPARATOR . 'IBootStrap.php';
    require_once 'BootStrap' . DIRECTORY_SEPARATOR . 'BootstrapInterface.php';
    require_once 'BootStrap' . DIRECTORY_SEPARATOR . 'ControllersBootstrap.php';
    require_once 'BootStrap' . DIRECTORY_SEPARATOR . 'ClassesBootstrap.php';
    require_once 'BootStrap' . DIRECTORY_SEPARATOR . 'StatisticsBootstrap.php';


    $new = new \Core\Bootstrap\ControllersBootstrap();
    $new->extractBuildData()->isExists()->instantiate();

    function __autoload($class_name){
        if ($type = \Core\Bootstrap\IBootStrap::getInstance()->getType($class_name, true)){
            list($loadFolder, $loadClass) = explode("\\", $class_name, 2);

            $loadFolder = ($loadFolder == 'Core' ? 'Classes' : $loadFolder);


            require_once constant($loadFolder . 'Folder') . $loadClass . ".php";

            $bootstrapLoader = '\\Core\\Bootstrap\\' . $type . 'Bootstrap';
            $bootstrap = new $bootstrapLoader();

            switch ($type){
                case 'Controllers':
                    /* @var $bootstrap \Core\Bootstrap\ControllersBootstrap */
                    /*$bootstrap->extractBuildData()->isExists()->instantiate();*/
                    break;
                case 'Classes':

                    list($path, $className) = explode("\\", $class_name);


                    require_once ClassesFolder . $className . ".php";


            }
        }
    }