<?php


class Config implements \ArrayAccess {

    protected $configuration = array();
    protected $file;
    protected $outputIni;
    protected $lastOutput;

    public function offsetExists($offset) {
        return isset($this->configuration[$offset]);
    }

    public function offsetGet($offset, $subOffset = null) {
        $current = $this->configuration[$offset];
        if (is_array($current) && $subOffset){
            return (!empty($current[$subOffset]) ? $current[$subOffset] : NULL);
        }

        return $current;
    }

    public function offsetSet($offset, $value) {

        $this->configuration[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->configuration[$offset]);
    }

    public function flush() {
        $this->configuration = array();
        return $this;
    }

    public function addYaml($yamlFile) {

        if (!file_exists($yamlFile)) {
            throw new \Exception('File "' . $yamlFile . '" not found');
        }

        $yamlConfig = yaml_parse_file($yamlFile);

        if (!$yamlConfig) {
            throw new \Exception('Could not parse "' . $yamlFile . '"');
        }

        $this->configuration = array_merge($this->configuration, $yamlConfig);

        return $this->configuration;
    }

    public function addIni($iniFile) {
        if (!file_exists($iniFile)) {
            throw new \Exception('File "' . $iniFile . '" not found');
        }

        $this->file = $iniFile;
        $iniConfig = parse_ini_file($iniFile, true);

        if (!$iniConfig) {
            throw new \Exception('Could not parse "' . $iniFile . '"');
        }

        foreach ($iniConfig as $sectionName => $values) {

            $sectionName = trim($sectionName);

            if (strpos($sectionName, ':') === false) {

                if (isset($this->configuration[$sectionName])) {
                    $previousConfig = $this->configuration[$sectionName];
                    $this->configuration[$sectionName] = array_merge($previousConfig, $values);
                } else {
                    $this->configuration[$sectionName] = $values;
                }

                continue;
            }

            $rawSectionNames = explode(':', $sectionName);
            $sectionNames = array_filter(array_map('trim', $rawSectionNames));

            if (count($sectionNames) != 2) {
                throw new \Exception('Invalid section definition "' . $sectionName . '" in "' . $iniFile . '"');
            }

            list($child, $parent) = $sectionNames;

            if (isset($this->configuration[$child])) {
                $sectionConfig = $this->configuration[$child];
            } else {
                $sectionConfig = array();
            }

            $parentlessConfig = array_merge($sectionConfig, $values);

            if (isset($this->configuration[$parent])) {
                $parentConfig = $this->configuration[$parent];
            } else {
                $parentConfig = array();
            }

            $sectionConfig = array_merge($parentConfig, $parentlessConfig);
            $this->configuration[$child] = $sectionConfig;
        }

        return $this;
    }

    public function addPhp($phpFile) {

        if (!file_exists($phpFile)) {
            throw new \Exception('File "' . $phpFile . '" not found');
        }

        ob_start();
        $phpConfig = include $phpFile;
        ob_end_clean();

        if (!is_array($phpConfig)) {
            $message = '"' . $phpFile . '" did not return any configuration. ' .
                'Expected: array, got: ' . gettype($phpConfig);
            throw new \Exception($message);
        }

        $this->configuration = array_merge($this->configuration, $phpConfig);

        return $this;
    }

    protected function readConfig($files){
        foreach ($files as $name => $values){
            if (is_array($values) || !$name){
                $this->lastOutput= $name;
                $str = "[" . $this->lastOutput . "]\n";
                file_put_contents($this->file, $str, FILE_APPEND);
                $this->readConfig($values);
                continue;
            }

            $str = $name . " = '" . $values . "'\n";
            file_put_contents($this->file, $str, FILE_APPEND);
        }
    }

    public function save(){
        $currentContent = $this->configuration;
        if(file_put_contents($this->file, '') === 0){
            $this->readConfig($currentContent);
        }
    }
}