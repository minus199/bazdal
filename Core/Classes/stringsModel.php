<?php
/**
 * Created by PhpStorm.
 * User: minus
 * Date: 4/15/14
 * Time: 1:59 PM
 */

namespace Core;

class stringsModel{
    public $refl;
    public $consts;
    public $rxmlSource;
    public $rxmlTarget;
    public $rxmlOutputs;
    public $sources;
    public $targets;
    public $outputs;

    protected $stringOutputSource;
    protected $stringOutputTarget;
    protected $sourcesKeys;
    protected $targetsKeys;


    protected $arrayToXML;
    protected $currentKey;
    protected $xmlWriter;
    protected $textToXMLarray;
    protected $textToXMLfile;

    public function __construct(){
        global $config;

        $this->consts = array();
        $this->refl = new \ReflectionClass('XMLReader');
        foreach ($this->refl->getConstants() as $k => $v) {
            $this->consts[$v] = $k;
        }

        /* Sources */
        $sourceFile = $config->offsetGet('files', 'file.dst.strings.src');
        $this->sources = ($sourceFile? $this->readFiles($sourceFile) : NULL);

        /* Targets */
        $targetFile = $config->offsetGet('files', 'file.dst.strings.target');
        $this->targets = ($targetFile ? $this->readFiles($targetFile) : NULL);
    }

    protected function _post($key = null){
        if ($key){
            return (isset($_POST[$key]) ? $_POST[$key] : NULL);
        }

        return $_POST;
    }

    protected function readFiles($file){
        global $config;

        if (!file_exists($file) && $file == $config->offsetGet('files', 'file.dst.strings.src') ){
            throw new Exception('File does not exist. aborting.');
        }

        $currentReader = new \XMLReader();
        $currentReader->open($file);

        /* Validate XML */
        if (!simplexml_load_string(file_get_contents($file))){
            /* If file is invalid but set as source, script cannot continue */
            if ($file == $config->offsetGet('files', 'file.dst.strings.src')){
                //echo json_encode(array('error' => 'invalid XML file.'));
                return;
            }

            /* If target file is invalid, truncate file and create a new file with same name */
            if ($file == $config->offsetGet('files', 'file.dst.strings.target')){
                $handle = fopen($file, 'w') or die('Cannot open file:  ' . $file);
                ftruncate($handle,0);
                return;
            }
        }

        $sources = array();
        while($currentReader->read()){
            $currentAttr = $currentReader->getAttribute('name');

            if (\XMLReader::LOADDTD === $currentReader->nodeType && isset($currentAttr)){
                $sources[$currentAttr] = array();
                $sources[$currentAttr]['untranslated'] = $currentReader->getAttribute('untranslated');
                $sources[$currentAttr]['value'] = $currentReader->readInnerXml();
            }
        }

        return (array)$sources;
    }

    protected function copyAlreadyTranslated(){
        global $config;
        $files = $config->offsetGet('files');
        $file = $files['file.dst.strings.target'];

        /* Truncate File Before Starting */
        $handle = fopen($file, 'w') or die('Cannot open file:  ' . $file);
        ftruncate($handle,0);

        /* Starting to write file */
        $xmlWriter = new XMLWriter();
        $xmlWriter->openUri($file);
        $xmlWriter->startDocument('1.0', 'utf-8');
            $xmlWriter->setIndent(true);
            $xmlWriter->startElement('resources');
                /* Start Writing String Elements */
                foreach ($this->sources as $offset => $value){
                    $xmlWriter->startElement('string');
                        $xmlWriter->writeAttribute('name', $offset);
                        if (!empty($this->targets[$offset]['value']) /* Translation Exist */){
                                $xmlWriter->writeAttribute('untranslated', 0);
                                /* Take offsets that has translation and puts it into output file */
                                $xmlWriter->text($this->targets[$offset]["value"]);
                        } else {
                            if (empty($this->sources[$offset]['value'])){
                                $xmlWriter->writeAttribute('untranslated', 0);
                            } else {
                                $xmlWriter->writeAttribute('untranslated', 1);
                                $xmlWriter->text('');
                            }
                        }
                    $xmlWriter->endElement();
                }
            $xmlWriter->endElement();
            /* End Writing String Elements */
        $xmlWriter->endDocument();

        return true;
    }

    protected function getFirstStringWithoutTranslation(){
        global $config;
        $file = $config->offsetGet('files', 'file.dst.strings.target');

        $doc = new \DOMDocument();
        $doc->loadXML(file_get_contents($file));
        $resources_node = $doc->getElementsByTagName("resources")->item(0);

        foreach ($resources_node->childNodes as $node /* @var $node DOMElement */ ) {
            if ($node instanceof \DOMElement){
                if ($node->getAttribute('untranslated') == 1){
                    $currentOffset = $node->getAttribute('name');
                    $currentValue = $this->sources[$currentOffset]['value'];
                    return array('offset' => $currentOffset, 'value' => $currentValue);
                }
            }
        }
    }

    protected function array2XML(array $array){
        $resources = array();

        foreach($array as $offset => $value){
            $resources['string'][] = array(
                '@value' => $value['value'],
                '@attributes' => array('name' => $offset, 'untranslated' => $value['untranslated'])
            );
        }

        $xmlOutput = Array2XML::createXML('resources', $resources);
        return $xmlOutput->saveXML();
    }

    protected function parseHTMLElement($element){
        $dom = new \DOMDocument('1.0', 'utf-8');
        $dom->loadHTML($element);//should be $element
        $dit = new \RecursiveIteratorIterator(new \RecursiveDOMIterator($dom), \RecursiveIteratorIterator::SELF_FIRST);

        $trimmedNodeValues = array();
        foreach($dit as $node) {
            /* @var $node \DOMElement */
            //$o[] = array($this->consts[$node->nodeType] => trim($node->nodeValue));
            $filteredValue = trim(str_replace("\n" ,"", trim(str_replace('"', "", implode(" ",array_filter(explode(" ", $node->nodeValue)))))));
            $trimmedNodeValues[] = $filteredValue;
        }

        $trimmedNodeValues = array_unique(array_filter($trimmedNodeValues));
        foreach ($trimmedNodeValues as $index => $value){
            echo  "\n". "(" . $index . ")" . "\n" . $value ;
        }
    }

    public function getStringsList(){
        global $config;
        $file = $config->offsetGet('files', 'file.dst.strings.src');

        $doc = new \DOMDocument();
        $doc->loadXML(file_get_contents($file));
        $resources_node = $doc->getElementsByTagName("resources")->item(0);

        $current = $this->getFirstStringWithoutTranslation();
        $output = array();

        foreach ($resources_node->childNodes as $node /* @var $node DOMElement */ ) {

            if ($node instanceof \DOMElement){
                $currentOffset = $node->getAttribute('name');
                $currentSourceValue = $this->sources[$currentOffset]['value'];
                $currentTargetValue = $this->targets[$currentOffset]['value'];
                //var_dump($currentSourceValue);

                $data = array('offset' => $currentOffset, 'sourceValue' => $currentSourceValue, 'targetValue' => $currentTargetValue);

                if ($current['offset'] == $data['offset']){
                    $output['current'] = $data;
                } else {
                    if (isset($output['current'])){
                        $output['after'][] = $data;
                    } else {
                        $output['before'][] = $data;
                    }
                }
            }
        }

        return $output;
    }
} 