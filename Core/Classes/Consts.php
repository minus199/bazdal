<?php
    $currentRelDir = array_filter(explode("/", $_SERVER['REQUEST_URI']));

    /* Paths */
    $currentRelDir = array_filter(explode("/", $_SERVER['REQUEST_URI']));

    /* Paths */
    define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . $config->offsetGet('env', 'base_path'));
    define('PUBLIC_PATH' , ROOT_PATH . "/public");
    define('ControllersFolder', ROOT_PATH . DIRECTORY_SEPARATOR . "Core" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR);
    define('ClassesFolder', ROOT_PATH . DIRECTORY_SEPARATOR . "Core" . DIRECTORY_SEPARATOR . "Classes" . DIRECTORY_SEPARATOR);
    define('StatisticsFolder', ROOT_PATH . DIRECTORY_SEPARATOR . "Core" . DIRECTORY_SEPARATOR . "Statistics" . DIRECTORY_SEPARATOR);

    /* URLS */
    define('BaseURL', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    define('PublicURL', BaseURL . "Public");